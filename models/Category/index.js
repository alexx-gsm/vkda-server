const mongoose = require('mongoose')
const Scheme = mongoose.Schema

const CategoryScheme = new Scheme({
  title: {
    type: String,
    required: true,
  },
  isRoot: {
    type: Boolean,
    default: false,
  },
  rootId: {
    type: String,
  },
  sorting: {
    type: Number,
    default: 0,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
})

module.exports = Category = mongoose.model('category', CategoryScheme)
