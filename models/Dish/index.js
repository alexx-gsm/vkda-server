const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create Schema
const DishSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  uom: {
    type: String,
  },
  weight: {
    type: String,
  },
  weight2: {
    type: String,
  },
  price: {
    type: String,
    required: true,
  },
  price2: {
    type: String,
    default: '',
  },
  extraPrices: {
    type: Array,
    default: [],
  },
  categoryId: {
    type: String,
  },
  recipe: {
    type: String,
  },
  comment: {
    type: String,
  },
  image: {
    type: String,
  },
  images: [
    {
      type: String,
    },
  ],
  link: {
    type: String,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
})

module.exports = Dish = mongoose.model('dish', DishSchema)
