const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = (data) => {
  let errors = {}

  // title
  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название блюда от 3 до 50 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  // Category
  data.categoryId = !isEmpty(data.categoryId) ? data.categoryId : ''
  if (Validator.isEmpty(data.categoryId)) {
    errors.categoryId = 'Категория обязательна'
  }

  // price
  data.price = !isEmpty(data.price) ? data.price : ''
  if (Validator.isEmpty(data.price)) {
    errors.price = 'Цена обязательна'
  }

  // price 2
  data.price2 = !isEmpty(data.price2) ? data.price2 : data.price

  // UOM
  data.uom = !isEmpty(data.uom) ? data.uom : ''

  // Weight
  data.weight = !isEmpty(data.weight) ? data.weight : ''

  // Weight2
  data.weight2 = !isEmpty(data.weight2) ? data.weight2 : data.weight

  // ExtraPrices
  data.extraPrices = !isEmpty(data.extraPrices) ? data.extraPrices : []

  // Recipe
  data.recipe = !isEmpty(data.recipe) ? data.recipe : ''

  // Comment
  data.comment = !isEmpty(data.comment) ? data.comment : ''

  // Image
  data.image = !isEmpty(data.image) ? data.image : ''

  // Link
  data.link = !isEmpty(data.link) ? data.link : ''

  // isDeleted
  data.isDeleted = !isEmpty(data.isDeleted) ? data.isDeleted : false

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
