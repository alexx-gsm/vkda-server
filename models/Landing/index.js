const mongoose = require('mongoose')
const Schema = mongoose.Schema

const LandingSchema = new Schema({
  week: {
    type: String,
    require: true,
  },
  days: {
    type: String,
    required: true,
  },
  complexDinners: {
    type: Object,
  },
  favoriteDishes: {
    type: Object,
  },
  // common fields
  isDeleted: {
    type: Boolean,
    default: false,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
})

module.exports = Landing = mongoose.model('landing', LandingSchema)
