const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function(data) {
  let errors = {}

  // week
  data.week = !isEmpty(data.week) ? data.week : ''
  if (isEmpty(data.week)) {
    errors.week = 'Где неделя?'
  }

  // days
  data.days = !isEmpty(data.days) ? data.days : ''

  // complexDinners
  data.complexDinners = !isEmpty(data.complexDinners) ? data.complexDinners : {}

  // favoriteDishes
  data.favoriteDishes = !isEmpty(data.favoriteDishes) ? data.favoriteDishes : {}

  // isDeleted
  data.isDeleted = !isEmpty(data.isDeleted) ? data.isDeleted : false

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
