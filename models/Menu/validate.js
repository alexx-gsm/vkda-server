const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = function validateMenuInput(data) {
  let errors = {}

  data.date = !isEmpty(data.date) ? data.date : ''
  if (Validator.isEmpty(data.date)) {
    errors.date = 'Дата меню обязательна'
  }

  // week
  data.week = !isEmpty(data.week) ? data.week : 0
  if (data.week === 0) {
    errors.week = 'Где неделя?'
  }

  data.dishes = !isEmpty(data.dishes) ? data.dishes : []
  if (isEmpty(data.dishes)) {
    errors.dishes = 'Блюда в меню просто необходимы'
  }

  data.link = !isEmpty(data.link) ? data.link : ''

  // isDeleted
  data.isDeleted = !isEmpty(data.isDeleted) ? data.isDeleted : false

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
