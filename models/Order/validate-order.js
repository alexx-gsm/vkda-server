const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = (data) => {
  let errors = {}

  // date
  data.date = !isEmpty(data.date) ? data.date : ''
  if (Validator.isEmpty(data.date)) {
    errors.date = 'Укажите дату доставки'
  }

  // customer
  data.customer = !isEmpty(data.customer) ? data.customer : ''

  // name
  data.name = !isEmpty(data.name) ? data.name : ''
  if (!Validator.isLength(data.name, { min: 5, max: 50 })) {
    errors.name = 'ФИО от 5 до 50 символов'
  }
  if (Validator.isEmpty(data.name)) {
    errors.name = 'ФИО обязательно'
  }

  // phone
  data.phone = !isEmpty(data.phone) ? data.phone : ''
  if (Validator.isEmpty(data.phone)) {
    errors.phone = 'Укажите телефон'
  }

  // address
  data.address = !isEmpty(data.address) ? data.address : ''
  if (Validator.isEmpty(data.address)) {
    errors.address = 'Укажите адрес доставки'
  }

  // email
  data.email = !isEmpty(data.email) ? data.email : ''

  // comment
  data.comment = !isEmpty(data.comment) ? data.comment : ''

  // total
  data.total = !isEmpty(data.total) ? data.total : 0

  // cart
  data.cart = !isEmpty(data.cart) ? data.cart : {}
  if (Object.keys(data.cart).length === 0) {
    errors.cart = 'Пустой заказ'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
