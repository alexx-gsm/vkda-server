const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = (data) => {
  let errors = {}

  // name
  data.name = !isEmpty(data.name) ? data.name : ''
  // if (!Validator.isLength(data.name, { min: 5, max: 30 })) {
  //   errors.name = 'ФИО от 5 до 30 символов'
  // }
  // if (Validator.isEmpty(data.name)) {
  //   errors.name = 'ФИО обязательно'
  // }

  // email
  data.email = !isEmpty(data.email) ? data.email : ''
  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email обязательно'
  }
  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email не верный формат'
  }

  // password
  data.password = !isEmpty(data.password) ? data.password : ''
  if (Validator.isEmpty(data.password)) {
    errors.password = 'Пароль обязателен'
  }
  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Пароль от 6 знаков'
  }

  // password 2
  data.password2 = !isEmpty(data.password2) ? data.password2 : ''
  if (Validator.isEmpty(data.password2)) {
    errors.password2 = 'Пароль обязателен'
  }
  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = 'Пароли не совпадают'
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
