const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VendorSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  person: {
    type: String,
  },
  phone: {
    type: String,
  },
  address: {
    type: String,
  },
  email: {
    type: String,
  },
  comment: {
    type: String,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
})

module.exports = Vendor = mongoose.model('vendor', VendorSchema)
