const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = (data) => {
  let errors = {}

  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название от 3 до 50 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  data.person = !isEmpty(data.person) ? data.person : ''

  data.phone = !isEmpty(data.phone) ? data.phone : ''

  data.address = !isEmpty(data.address) ? data.address : ''

  data.email = !isEmpty(data.email) ? data.email : ''
  if (!isEmpty(data.email) && !Validator.isEmail(data.email)) {
    errors.email = 'Email не верный формат'
  }

  data.comment = !isEmpty(data.comment) ? data.comment : ''

  data.isDeleted = !isEmpty(data.isDeleted) ? data.isDeleted : false

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
