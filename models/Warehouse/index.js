const mongoose = require('mongoose')
const Schema = mongoose.Schema

const WarehouseSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  isNegativeBalance: {
    type: Boolean,
    default: true,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
})

module.exports = Warehouse = mongoose.model('warehouse', WarehouseSchema)
