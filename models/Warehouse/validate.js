const Validator = require('validator')
const isEmpty = require('../../helpers/is-empty')

module.exports = data => {
  let errors = {}

  data.title = !isEmpty(data.title) ? data.title : ''
  if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
    errors.title = 'Название от 3 до 50 символов'
  }
  if (Validator.isEmpty(data.title)) {
    errors.title = 'Название обязательно'
  }

  data.isNegativeBalance = !isEmpty(data.isNegativeBalance)
    ? data.isNegativeBalance
    : true

  data.isDeleted = !isEmpty(data.isDeleted) ? data.isDeleted : false

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
