const express = require('express')
const router = express.Router()
const passport = require('passport')

const Category = require('../../models/Category')
const validateInput = require('../../models/Category/validate')

/**
 * !   @route   POST api/categories/all
 * *   @desc    Get all categories
 * !   @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      res.json(await Category.find({ isDeleted: false }).sort({ sorting: 1 }))
    } catch (error) {
      res.status(400).json(error)
    }
  }
)

/**
 * !   @route   POST api/categories/dish
 * *   @desc    Get dish categories
 * *   @access  Public
 */
router.post('/dish', async (req, res) => {
  try {
    const dishCategory = await Category.findOne({ title: 'Блюда' })
    res.json(
      await Category.find({ rootId: dishCategory._id, isDeleted: false }).sort({
        sorting: 1,
      })
    )
  } catch (error) {
    res.status(400).json(error)
  }
})

/**
 * ! @route   PUT api/categories
 * * @desc    Save category
 * ! @access  Private
 */
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validateInput(req.body)

    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body
    const itemFields = {
      title: req.body.title,
      isRoot: req.body.isRoot,
      rootId: req.body.rootId,
      sorting: req.body.sorting,
      isDeleted: req.body.isDeleted,
    }

    try {
      if (_id) {
        res.json(
          await Category.findOneAndUpdate(
            { _id },
            { $set: { ...itemFields, updated: new Date() } },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Category(itemFields)
        res.json(await newItem.save())
      }
    } catch (error) {
      res.status(400).json(error)
    }
  }
)

/**
 * ! @route   POST api/categories/:id
 * * @desc    Get category by id
 * ? @param   _id: Vendor Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Category.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   DELETE api/categories/:id
 * * @desc    Delete category
 * ! @access  Private
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const { _id } = req.params

      if (_id && req.user.role === 'admin') {
        await Category.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
        res.json('ok')
      } else {
        res.status(404).json('ops')
      }
    } catch (error) {
      res.status(404).json(error)
    }
  }
)

module.exports = router
