const express = require('express')
const router = express.Router()
const passport = require('passport')

const multer = require('multer')
const uploadDest = multer({ dest: 'uploads/' })
const fs = require('fs')

const Dish = require('../../models/Dish')
const validateInput = require('../../models/Dish/validate')

/**
 * ! @route   POST api/dishes/all
 * * @desc    Get all dishes
 * * @access  Public
 */
router.post('/all', async (req, res) => {
  try {
    res.json(await Dish.find({ isDeleted: false }).sort({ title: 1 }))
  } catch (error) {
    res.status(400).json(error)
  }
})

/**
 * ! @route   PUT api/dishes
 * * @desc    Save dish
 * ! @access  Private
 */
router.put(
  '/',
  [
    passport.authenticate('jwt', { session: false }),
    uploadDest.single('upload'),
  ],
  async (req, res) => {
    const { errors, isValid } = validateInput(req.body)

    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { upload } = req.body
    let { image } = req.body

    if (upload) {
      const base64image = upload[0].replace(/^data:image\/jpeg;base64,/, '')
      image = `/images/dish-${Date.now()}.jpeg`

      fs.writeFile(`public${image}`, base64image, 'base64', function(err) {
        console.log(err)
      })
    }

    const { _id } = req.body

    const itemFields = {
      image,
      title: req.body.title,
      categoryId: req.body.categoryId,
      price: req.body.price,
      price2: req.body.price2,
      uom: req.body.uom,
      weight: req.body.weight,
      weight2: req.body.weight2,
      extraPrices: req.body.extraPrices,
      recipe: req.body.recipe,
      comment: req.body.comment,
      link: req.body.link,
      isDeleted: req.body.isDeleted,
    }

    try {
      if (_id) {
        res.json(
          await Dish.findOneAndUpdate(
            { _id },
            { $set: { ...itemFields, updated: new Date() } },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Dish(itemFields)
        res.json(await newItem.save())
      }
    } catch (error) {
      console.log('error', error)
      res.status(400).json(error)
    }
  }
)

/**
 * !  @route   GET api/dishes/:id
 * *  @desc    Get dish by id
 * *  @access  Public
 */
router.post('/:_id', async (req, res) => {
  const { _id } = req.params

  try {
    if (_id) {
      res.json(await Dish.findOne({ _id }))
    }
  } catch (error) {
    res.status(403).json(error)
  }
})

/**
 *! @route   DELETE api/dishes/:id
 ** @desc    Delete dish by id
 *! @access  Privite
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    if (req.user.role !== 'admin') {
      res.status(401).json({ noadmin: 'only admin can delete dish' })
    }

    const { _id } = req.params

    try {
      if (_id) {
        await Dish.deleteOne({ _id })
        res.json({})
      }
    } catch (error) {
      res.status(403).json(error)
    }
  }
)

module.exports = router
