const express = require('express')
const router = express.Router()
const passport = require('passport')
const moment = require('moment')
moment.locale('ru')

const Landing = require('../../models/Landing')
const validateInput = require('../../models/Landing/validate')

/**
 * ! @route   POST api/landings/all
 * * @desc    Get all landings
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      res.json(await Landing.find({ isDeleted: false }).sort({ week: -1 }))
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   PUT api/landings
 * * @desc    Save landing
 * ! @access  Private
 */
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validateInput(req.body)

    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id, week, days, complexDinners, favoriteDishes } = req.body

    const itemFields = {
      week,
      days,
      complexDinners,
      favoriteDishes,
    }

    try {
      if (_id) {
        res.json(
          await Landing.findOneAndUpdate(
            { _id },
            { $set: { ...itemFields, updated: new Date() } },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Landing(itemFields)
        res.json(await newItem.save())
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/landings/today
 * * @desc    Get today landing
 * * @access  Public
 */
router.post('/today', async (req, res) => {
  const { today } = req.body

  try {
    if (today) {
      const dayToday = moment(today, 'L')
      const tomorrow =
        dayToday.weekday() < 4
          ? dayToday.add(1, 'd').format('L')
          : dayToday
              .add(1, 'w')
              .startOf('week')
              .format('L')

      const todayLanding = await Landing.findOne({ date: today })
      const tomorrowLanding = await Landing.findOne({ date: tomorrow })
      res.json({ todayLanding, tomorrowLanding })
    }
  } catch (err) {
    res.status(400).json(err)
  }
})

/**
 * ! @route   POST api/landings/:id
 * * @desc    Get Landing by id
 * ? @param   id: landing Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Landing.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 *! @route   DELETE api/landings/:id
 ** @desc    Delete Landing by id
 *! @access  Privite
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    if (req.user.role !== 'admin') {
      res.status(401).json({ noadmin: 'only admin can delete Landing' })
    }

    const { _id } = req.params

    try {
      if (_id) {
        await Landing.deleteOne({ _id })
        res.json({})
      }
    } catch (error) {
      res.status(403).json(error)
    }
  }
)

/**
 * ! @route   POST api/landings/weekly/:week
 * * @desc    Get all landings
 * ? @access  Private
 */
// router.post(
//   '/weekly/:week',
//   // passport.authenticate('jwt', { session: false }),
//   async (req, res) => {
//     // TODO: select landing in interval
//     const { week } = req.params

//     try {
//       if (Number(week) > 0) {
//         res.json(await Landing.find({ week }).sort({ date: 1 }))
//       }
//     } catch (err) {
//       res.status(400).json(err)
//     }
//   }
// )

/**
 * ! @route   POST api/landings/pdf
 * * @desc    Create pdf landing
 * ? @access  Private
 */
// router.post(
//   '/pdf',
//   passport.authenticate('jwt', { session: false }),
//   async (req, res) => {
//     //  validate input data
//     const { errors, isValid } = validateLandingInput(req.body)
//     if (!isValid) {
//       return res.status(400).json(errors)
//     }

//     const { _id, date, dishes, link } = req.body

//     const itemFields = {
//       date,
//       dishes,
//       link,
//     }

//     try {
//       // let newItem = {}
//       // if (_id) {
//       //   newItem = await Landing.findOneAndUpdate(
//       //     { _id },
//       //     { $set: itemFields },
//       //     { new: true, upsert: true }
//       //   )
//       // } else {
//       //   _newItem = new Landing(itemFields)
//       //   newItem = await newItem.save()
//       // }
//       res.json({})
//     } catch (err) {
//       res.status(400).json(err)
//     }
//   }
// )

module.exports = router
