const express = require('express')
const router = express.Router()
const passport = require('passport')
const moment = require('moment')
moment.locale('ru')

const Menu = require('../../models/Menu')
const validateInput = require('../../models/Menu/validate')

/**
 * ! @route   POST api/menus/all
 * * @desc    Get all menus
 * ! @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      res.json(await Menu.find({ isDeleted: false }).sort({ updated: -1 }))
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   PUT api/menus
 * * @desc    Save menu
 * ! @access  Private
 */
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validateInput(req.body)

    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id, date, week, dishes, link } = req.body
    const itemFields = {
      date,
      week,
      dishes,
      link,
    }

    try {
      if (_id) {
        res.json(
          await Menu.findOneAndUpdate(
            { _id },
            { $set: { ...itemFields, updated: new Date() } },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Menu(itemFields)
        newItem.save().then((item) => res.json(item))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   POST api/menus/today
 * * @desc    Get today menu
 * * @access  Public
 */
router.post('/today', async (req, res) => {
  const { today } = req.body

  try {
    if (today) {
      const dayToday = moment(today, 'L')
      const tomorrow =
        dayToday.weekday() < 4
          ? dayToday.add(1, 'd').format('L')
          : dayToday
              .add(1, 'w')
              .startOf('week')
              .format('L')

      const todayMenu = await Menu.findOne({ date: today })
      const tomorrowMenu = await Menu.findOne({ date: tomorrow })
      res.json({ todayMenu, tomorrowMenu })
    }
  } catch (err) {
    res.status(400).json(err)
  }
})

/**
 * ! @route   POST api/menus/:id
 * * @desc    Get Menu by id
 * ? @param   id: menu Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Menu.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 *! @route   DELETE api/menus/:id
 ** @desc    Delete Menu by id
 *! @access  Privite
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    if (req.user.role !== 'admin') {
      res.status(401).json({ noadmin: 'only admin can delete Menu' })
    }

    const { _id } = req.params

    try {
      if (_id) {
        await Menu.deleteOne({ _id })
        res.json({})
      }
    } catch (error) {
      res.status(403).json(error)
    }
  }
)

/**
 * ! @route   POST api/menus/weekly/:week
 * * @desc    Get all menus
 * ? @access  Private
 */
// router.post(
//   '/weekly/:week',
//   // passport.authenticate('jwt', { session: false }),
//   async (req, res) => {
//     // TODO: select menu in interval
//     const { week } = req.params

//     try {
//       if (Number(week) > 0) {
//         res.json(await Menu.find({ week }).sort({ date: 1 }))
//       }
//     } catch (err) {
//       res.status(400).json(err)
//     }
//   }
// )

/**
 * ! @route   POST api/menus/pdf
 * * @desc    Create pdf menu
 * ? @access  Private
 */
// router.post(
//   '/pdf',
//   passport.authenticate('jwt', { session: false }),
//   async (req, res) => {
//     //  validate input data
//     const { errors, isValid } = validateMenuInput(req.body)
//     if (!isValid) {
//       return res.status(400).json(errors)
//     }

//     const { _id, date, dishes, link } = req.body

//     const itemFields = {
//       date,
//       dishes,
//       link,
//     }

//     try {
//       // let newItem = {}
//       // if (_id) {
//       //   newItem = await Menu.findOneAndUpdate(
//       //     { _id },
//       //     { $set: itemFields },
//       //     { new: true, upsert: true }
//       //   )
//       // } else {
//       //   _newItem = new Menu(itemFields)
//       //   newItem = await newItem.save()
//       // }
//       res.json({})
//     } catch (err) {
//       res.status(400).json(err)
//     }
//   }
// )

module.exports = router
