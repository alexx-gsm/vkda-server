const express = require('express')
const router = express.Router()
const passport = require('passport')

const validateInput = require('../../models/Vendor/validate')
const Vendor = require('../../models/Vendor')

/**
 * !   @route   POST api/vendors/all
 * *   @desc    Get all vendors
 * *   @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      res.json(await Vendor.find({ isDeleted: false }))
    } catch (error) {
      res.status(400).json(error)
    }
  }
)

/**
 * ! @route   PUT api/vendors
 * * @desc    Save vendor
 * * @access  Private
 */
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validateInput(req.body)

    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id } = req.body
    const itemFields = {
      title: req.body.title,
      person: req.body.person,
      phone: req.body.phone,
      address: req.body.address,
      email: req.body.email,
      comment: req.body.comment,
      isDeleted: req.body.isDeleted,
    }

    try {
      if (_id) {
        res.json(
          await Vendor.findOneAndUpdate(
            { _id },
            { $set: { ...itemFields, updated: new Date() } },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Vendor(itemFields)
        res.json(await newItem.save())
      }
    } catch (error) {
      console.log('error', error)
      res.status(400).json(error)
    }
  }
)

/**
 * ! @route   POST api/vendors/:id
 * * @desc    Get Vendor by id
 * ? @param   id: Vendor Id
 * ! @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Vendor.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 * ! @route   DELETE api/vendors/:id
 * * @desc    Delete vendor
 * ! @access  Private
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const { _id } = req.params
      if (_id && req.user.role === 'admin') {
        await Vendor.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
        res.json('ok')
      } else {
        res.status(404).json('ops')
      }
    } catch (error) {
      console.log('error', error)
      res.status(404).json(error)
    }
  }
)

module.exports = router
