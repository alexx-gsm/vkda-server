const express = require('express')
const router = express.Router()
const passport = require('passport')

const validateInput = require('../../models/Warehouse/validate')
const Warehouse = require('../../models/Warehouse')

/**
 * ! @route   PUT api/warehouses
 * * @desc    Save warehouse
 * * @access  Private
 */
router.put(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validateInput(req.body)

    if (!isValid) {
      return res.status(400).json(errors)
    }

    const { _id, title, isNegativeBalance, isDeleted } = req.body
    const itemFields = {
      title,
      isNegativeBalance,
      isDeleted,
    }

    try {
      if (_id) {
        res.json(
          await Warehouse.findOneAndUpdate(
            { _id },
            { $set: { ...itemFields, updated: new Date() } },
            { new: true, upsert: true }
          )
        )
      } else {
        const newItem = new Warehouse(itemFields)
        res.json(await newItem.save())
      }
    } catch (error) {
      console.log('error', error)
      res.status(400).json(error)
    }
  }
)

/**
 * !   @route   POST api/warehouses/all
 * *   @desc    Get All Warehouses
 * *   @access  Private
 */
router.post(
  '/all',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      res.json(await Warehouse.find({ isDeleted: false }))
    } catch (error) {
      res.status(400).json(error)
    }
  }
)

/**
 * ! @route   POST api/warehouses/:id
 * * @desc    Get Warehouse by id
 * * @param   id: Warehouse Id
 * * @access  Private
 */
router.post(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { _id } = req.params

    try {
      if (_id) {
        res.json(await Warehouse.findOne({ _id }))
      }
    } catch (err) {
      res.status(400).json(err)
    }
  }
)

/**
 *! @route   DELETE api/warehouses/:id
 ** @desc    Delete warehouse
 ** @access  Private
 */
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const { _id } = req.params
      if (_id && req.user.role === 'admin') {
        await Warehouse.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
        res.json('ok')
      } else {
        res.status(404).json('ops')
      }
    } catch (error) {
      console.log('error', error)
      res.status(404).json(error)
    }
  }
)

module.exports = router
