const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')
const cors = require('cors')

const app = express()

// CORS middleware
// app.use(cors(corsOptions))
app.use(cors())
// public folder
app.use(express.static('public'))
// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// DB config
const db = require('./config/keys').mongoURI

// MongoDB
mongoose
  .connect(db, { useNewUrlParser: true, useFindAndModify: false })
  .then(() => console.log('MongoDB Connected'))
  .catch((err) => console.log(err))

// register User model
require('./models/User')
// Passport
app.use(passport.initialize())
require('./config/passport')(passport)

// Use Routes
app.use('/api/users', require('./routes/api/users'))
app.use('/api/warehouses', require('./routes/api/warehouses'))
app.use('/api/vendors', require('./routes/api/vendors'))
app.use('/api/categories', require('./routes/api/categories'))
app.use('/api/dishes', require('./routes/api/dishes'))
app.use('/api/menus', require('./routes/api/menus'))
app.use('/api/orders', require('./routes/api/orders'))
app.use('/api/landings', require('./routes/api/landings'))

const port = process.env.PORT || 5501

app.listen(port, () => console.log(`Server running on port ${port}`))
